#railwaystation {
  [zoom>13]{
    marker-fill: #841b0d;
    marker-line-width: 0;
    marker-line-color: #969696;
    marker-width: 4;
    [zoom=15]{
        marker-width: 5;
        marker-line-width: 1;
     }  
    [zoom=16]{
        marker-width: 7;
        marker-line-width: 2;
     }     
    [zoom=17]{
        marker-width: 12;
        marker-line-width: 3;
     }      
    [zoom>17]{
      marker-width: 20;
      marker-line-width: 4;
     }
  }
}


#motorwayjunction {

  marker-file: url("images/shields/motorway-junction.svg");  
  text-allow-overlap: true;
  text-face-name:@font_bold;
  text-name:"[junctnum]";
  text-size:20;
  text-fill:#63c8dd;
  text-transform:uppercase;
  marker-transform:scale(1,1);
  marker-avoid-edges: true;

  [zoom<16]{
    text-size:10;
    marker-transform:scale(0.5,0.5);
  }
  
  [zoom<14]{
      text-name:"['']";
      marker-file: url();
  }
}
